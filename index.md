---
layout: default
title: Home
order: 1
---


# DAISY - introduction
DAta Information SYstem is an open-source web application that allows biomedical research institutions to map their data and data flows in accordance with General Data Protection Regulation (GDPR) requirements.    
DAISY is a tool, created in response to the novel principle - *accountability*, introduced by the GDPR in May 2018. Accountability requires the demonstration of the compliance with all data protection principles and moreover, to record all data processing. DAISY fulfils GDPR requirements effectively, what we observe by analysing the transparency and record-keeping obligations of each GDPR principle. DAISY as well enhances the collaboration between partners and allows the institutions to create their GDPR data registers. DAISY is a tool tailored specifically for the biomedical research, supporting their complex data flows and tagging projects with controlled vocabulary terms to denote the study features.

The application is available as a free and open source tool on [Github](https://github.com/elixir-luxembourg/daisy/) and the issue tracker is open to everyone.

DAISY is actively being used at the [Luxembourg Centre for Systems Biomedicine](https://wwwen.uni.lu/lcsb) and the [ELIXIR-Luxembourg](https://elixir-luxembourg.org) data hub.


## License
DAISY is licensed under [GNU Affero General Public License v3.0 (AGPL 3.0)](https://www.gnu.org/licenses/agpl-3.0.en.html).  

DAISY development is undertaken by [ELIXIR-Luxembourg](https://elixir-luxembourg.org).


## Citation
When using or referring to DAISY, please cite our publication:

__DAISY: A Data Information System for accountability under the General Data Protection Regulation.__ GigaScience, Volume 8, Issue 12, December 2019, [giz140](https://doi.org/10.1093/gigascience/giz140).


## Source code
The source code and issue tracking for DAISY is available on [Github](https://github.com/elixir-luxembourg/daisy/).


<!--
Find DAISY description in [bio.tools service](https://bio.tools/Data_Information_System_DAISY).

[ToolPool](https://www.toolpool-gesundheitsforschung.de/produkte/data-information-system-daisy) -->
