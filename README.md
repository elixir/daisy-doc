# Daisy documentation

## How to get started

1. clone the repository

2. install dependencies using bundler

```bash
bundle install --path vendor/bundle
```

1. run server locally

```bash
bundle exec jekyll server
```

