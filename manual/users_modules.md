

# 2 What records can be kept with DAISY?

This section contains a brief description of DAISY functions listed in the application's menu bar (image below) and some tips how to effectively familiarise with DAISY application.     

<br>
<span style="display:block; text-align:center">![Alt](../img/menubar.png "DAISY Menu bar"){:width="800px"}<br/><small>DAISY Menu bar</small></span>
<br>

## 2.1 Projects
Projects Management module allows for the recording of research activities as projects. Documenting projects is critical for GDPR compliance as projects constitute the purpose and the context of use of the personal data.    
Any document supporting the legal and ethical basis for data use can be stored in DAISY (e.g. ethics approvals, consent configurations or subject information sheets). [**Go to Project Management**]({{ "/manual/project_management_details/" | relative_url }})  

## 2.2 Datasets

Datasets Management module allows for the recording of personal data held by the institution. The dataset may or may not fall in the context of a particular project. DAISY allows datasets to be defined in a granular way; where - if desired - each data subset, called a *data declaration*, can be listed individually. These declarations may list data from a particular partner, data of a particular cohort or data of a particular type.
[**Go to Dataset Management**]({{ "/manual/dataset_management_details" | relative_url }})


## 2.3 Contracts

Contracts Management module allows for the recording and storage of legal contracts of various types that have been signed with partner institutes or suppliers. Consortium agreements, data sharing agreements, material transfer agreements are the examples of the contracts.
 <!-- For GDPR compliance the contracts become useful when documenting the source of received datasets or the target datasets transferred. -->
 For GDPR compliance the contracts become useful in case of documenting the received datasets source or transferred datasets target. [**Go to Contracts Management**]({{ "/manual/contract_management_details" | relative_url }})


## 2.4 Definitions
Definitions Management module allows the maintenance of secondary entities, which are used when defining the contracts, projects or datasets. Users can manage cohorts, partner institutes and contact persons via the definitions module.
[**Go to Definitions Management**]({{ "/manual/definitions_management_details" | relative_url }})


---
<div style="text-align: right"> <strong><a href="#top">Back to top</a></strong></div>
<br />


# 3 Different types of DAISY users

Any user with an account can login to DAISY and start creating records.  Users that create a record become the record's *owner* and will be able to change and delete the record at any time.
In DAISY, a records owner, however, is not the one with the utmost privileges. DAISY provides various types of users accounts, and associated privileges.

<br/>

<!-- This is the default role assigned to all users. All DAISY users can view all Dataset, Project, Contract and Definitions. The document attachments of records are excluded from this view permission. -->
- **Standard user**    
The default type of user is a standard user. Standard users can:
	- view any *Dataset*, *Project*, *Contract* or *Definition* record in DAISY, including those created by others. The documents attachments on the records are, however, protected, and they are not visible to other standard users.
	- create records of their own.
	- edit and delete records of their own.    

- **VIP user**    
The research principle investigators are VIP type users. Whenever a *Dataset*, *Project*, *Contract*  record gets created in DAISY, a VIP user **must be designated** as the record's **Local Custodian**. Records cannot be created without a local custodian.
In addition to the privileges of the standard user, the VIP Users have the following rights:
	- view, edit and delete records under his custodianship,
	- view and manage the document attachments of records under their custodianship,
	- grant other users permissions on the records under his custodianship.

- **Legal user**  
The users assigned to this group can are allowed to manage *Contract* records. Legal personnel can:
	- add, view, edit and remove any contract.
	- grant the other users with an access for the contract.
	- view all records in DAISY and manage their documents attachments.    
	<br />

<br>
For more details go to [Users Groups and Permissions]({{ "/manual/user_management_details/" | relative_url }}) (recommended for those administering DAISY deployments).


---
<div style="text-align: right"> <strong><a href="#top">Back to top</a></strong></div>
<br />
